# Circular UI Widget #

Simple circular widget created on pure JavaScript by using Canvas.

# Known issues #

* When picking the value, there is no stopping when reaching the end of the circle.
* [FIXED] When picking the value, there are troubles with displaying an empty/full circle.
* (Desktop) IE recognizes movement correctly only when on mouse click/hold is dragged out of the main div.