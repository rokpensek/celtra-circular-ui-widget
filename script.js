var CircularWidget = function () {
	var canvas;
	
	// Test to see if the code is running on a touch device
	var isTouchSupported = 'ontouchstart' in window.document;
	var startEvent = isTouchSupported ? 'touchstart' : 'mousedown';
	var moveEvent = isTouchSupported ? 'touchmove' : 'mousemove';
	var endEvent = isTouchSupported ? 'touchend' : 'mouseup';
	var isMouseDown=false;
	document.addEventListener(endEvent,function(e){isMouseDown = false;e.preventDefault();},false);
	document.addEventListener(moveEvent,function(e){
	if(isMouseDown) 
		move(e);
	e.preventDefault();
	},false);

	/**
	 * Constructor that takes options and calls the necessary
	 * functions to create circles, knobs etc.
	 * @constructor
	 */
	function createPrivate(options){

		canvas = {
			width: options.radius*2,
			height: options.radius*2,
			top: 0,
			left: 0,
			minVal: options.min,
			maxVal: options.max,
			steps: options.step,
			calcAngleStep: options.max/options.step,
			color: options.color,
			container: options.container,
			randomId: 0
		};
		
		//generate id
		randomNum();
		
		createCircle(options.container, 'dashed','#cecece');
		createCircle(options.container, 'solid','#cecece');
		
		//create an empty value field
		createValue();
	}
	
	/**
	 * Method to randomize a unique number to be used as ID in DOM
	 * @method
	 */
	function randomNum(){
		do{
			canvas.randomId = Math.floor((Math.random() * 10000) + 1);
		}
		while(document.getElementById('Circle'+canvas.randomId)!=null || document.getElementById('knob'+canvas.randomId)!=null);
	}
	
	/**
	 * Method that creates and positions the newly created knob
	 * on start position of the circle
	 * @method
	 */
	function createKnob(element){
		var knob = document.createElement('div');
		knob.id = "knob"+canvas.randomId;
		knob.className = "pointer";
		document.getElementById(element).appendChild(knob);
		document.getElementById("knob"+canvas.randomId).addEventListener(startEvent,function(e){isMouseDown = true;e.preventDefault();},false);
		
		//setup pointer at the starting position
		document.getElementById(knob.id).style.left = (document.getElementById(canvas.container).offsetWidth/2)+"px";
		document.getElementById(knob.id).style.top = (document.getElementById(canvas.container).offsetHeight/2)-(canvas.height/2)+10+"px";
	}
	
	/**
	 * Method that creates canvas element and calls
	 * appropriate function to render specific circle
	 * based on "control" variable.
	 * @method
	 */
	function createCircle(element, control, color, angle){

		var canvasEl = document.createElement('canvas');
		canvasEl.id = "Circle"+canvas.randomId;

		canvasEl.width = canvas.width;
		canvasEl.height = canvas.height;

		canvas.center = [canvas.top + canvas.height / 2,canvas.left + canvas.width / 2];
		canvas.radius = canvas.width / 2;
		
		var ctx = canvasEl.getContext('2d');
		ctx.save();
		ctx.fillStyle = color;
		ctx.translate(canvas.width / 2, canvas.height / 2);      // move to center of circle
		
		switch(control){
		case 'dashed':	
			canvasEl.id = "Circle"+canvas.randomId;
			genDashBorders(canvas, ctx);
			break;
		case 'solid':
			createKnob(element);
			canvasEl.id = "CircleColored"+canvas.randomId;
			ctx.globalAlpha = 0.6;
			genSolidBorders(canvas, ctx, color, angle);
			break;
		}
		
		//append new circle
		document.getElementById(element).appendChild(canvasEl);
		//center it
		centerCircle(canvasEl.id);
	}
	
	/**
	 * Method that positions a circle to center
	 * @method
	 */
	function centerCircle(nameID){
		document.getElementById(nameID).style["margin-top"]=(document.getElementById(canvas.container).offsetHeight/2)-(canvas.height / 2)+"px";
		document.getElementById(nameID).style["margin-left"]=(document.getElementById(canvas.container).offsetWidth/2)-(canvas.width / 2)+"px";
	}
	
	/**
	 * Method that re-renders the circle based
	 * on changed input data
	 * @method
	 */
	function updateCircle(element, control, color, angle){
		
		var canvasEl = document.getElementById(element);
		var ctx = canvasEl.getContext('2d');
		
		ctx.clearRect(-canvas.width / 2, -canvas.height / 2, canvas.width, canvas.height);
		
		ctx.save();
		ctx.fillStyle = color;
		
		ctx.translate(canvas.top, canvas.left);      // move to center of circle
		
		switch(control){
		case 'dashed':	
			genDashBorders(canvas, ctx);
			break;
		case 'solid':
			ctx.globalAlpha = 0.7;
			genSolidBorders(canvas, ctx, color, angle);
			break;
		}
		ctx.restore();
	}

	/**
	 * Method that renders a circle with desired dashed style
	 * @method
	 */
	function genDashBorders(canvas, ctx){
		var r1 = Math.min(canvas.width, canvas.height) * 0.5;    // outer radius
		var r0 = r1 - 20;                 // inner radius
		
		var n = canvas.width/2;
		var theta = 2 * Math.PI / n;
		var phi = theta * 0.40;           // relative half-block width
		
		for (var i = 0; i < n; ++i) {
			ctx.beginPath();
			ctx.arc(0, 0, r0, -phi, phi);
			ctx.arc(0, 0, r1, phi, -phi, true);
			ctx.fill();
			ctx.rotate(theta);            // rotate the coordinates by one block
		}
	}
	
	/**
	 * Method that renders solid borders to show progress effect
	 * @method
	 */
	function genSolidBorders(canvas, ctx, color, angle){
		var r1 = Math.min(canvas.width, canvas.height) * 0.5;    // outer radius
		var r0 = r1 - 10;                 // inner radius
		
		var theta = angle-(90 * (Math.PI/180));
		
		ctx.beginPath();
		ctx.arc(0, 0, r0, 270 * (Math.PI/180), -theta);
		ctx.lineWidth=20;
		
		ctx.strokeStyle=color;
		ctx.stroke();
	}
	
	/**
	 * Method that gets the mouse position and moves the knob position
	 * to the correct position on circle border. Also calls functions
	 * to update progress circle
	 * @method
	 */
	function move(e) {
		var result=0;
		if(isTouchSupported){
			if (e.touches[0].x != undefined && e.touches[0].y != undefined)
				result = limit(e.touches[0].x, e.touches[0].y);
			else //browsers issue that don't have x and y properties
			{
				x = e.touches[0].clientX + document.body.scrollLeft +
				  document.documentElement.scrollLeft;
				y = e.touches[0].clientY + document.body.scrollTop +
				  document.documentElement.scrollTop;
				 result = limit(x, y);
			}
		}
		else
		{
			if (e.x != undefined && e.y != undefined)
				result = limit(e.x, e.y);
			else //firefox issue
			{
				x = e.clientX + document.body.scrollLeft +
				  document.documentElement.scrollLeft;
				y = e.clientY + document.body.scrollTop +
				  document.documentElement.scrollTop;
				 result = limit(x, y);
			}
		}
		var pointer = document.getElementById("knob"+canvas.randomId);
		//add offset
		pointer.style.left = (document.getElementById(canvas.container).offsetWidth/2)-(canvas.width / 2)+result.x + "px";
		pointer.style.top = (document.getElementById(canvas.container).offsetHeight/2)-(canvas.height/2) + result.y + "px";
	}
	
	/**
	 * Function that remaps values from one min to max value
	 * and keeps the same steps
	 * @function
	 */
	function remap(value, low1, high1, low2, high2){
		return low2 + (-value - low1) * (high2 - low2) / (high1 - low1);
	}
	
	/**
	 * Function that calculates the step angle in circle and 
	 * enables to lock in between values
	 * @function
	 */
	function stepFunc(curValue, step)
	{
		var step_angle=(360/step) * (Math.PI/180);
		var step_value=(canvas.maxVal/step);
		//console.log(curValue+" "+step_angle+" "+step_value);
		if(curValue > 0 && curValue<(Math.PI-step_angle))
		{
			curVal=remap(curValue, -Math.PI, Math.PI, canvas.minVal, canvas.maxVal);
			curVal = Math.floor(curVal/step_value) * step_value;
			document.getElementById("value"+canvas.randomId).innerHTML="$"+curVal;
			return Math.floor(curValue/step_angle) * step_angle;
		}
		else if(curValue < 0 && curValue > (-Math.PI+step_angle) )
		{
			curVal=remap(curValue, -Math.PI, Math.PI, canvas.minVal, canvas.maxVal);
			curVal = Math.ceil(curVal/step_value) * step_value;
			document.getElementById("value"+canvas.randomId).innerHTML="$"+curVal;
			return Math.ceil(curValue/step_angle) * step_angle;
		}
		else if(curValue > 0){
			curVal=remap(Math.PI, -Math.PI, Math.PI, canvas.minVal, canvas.maxVal);
			curVal = Math.ceil(curVal/step_value) * step_value;
			document.getElementById("value"+canvas.randomId).innerHTML="$"+curVal;
			return -Math.PI;
		}
		else if(curValue < 0){
			curVal=remap(curValue, -Math.PI, Math.PI, canvas.minVal, canvas.maxVal);
			curVal = Math.ceil(curVal/step_value) * step_value;
			document.getElementById("value"+canvas.randomId).innerHTML="$"+curVal;
			return Math.PI;
		}
	}
	
	/**
	 * Function that limits the returned x, y position
	 * on the border line of the circle and updates the circle
	 * progress bar
	 * @function
	 */
	function limit(x, y) {
	
			//console.log(canvas.center[0]);
			element = document.getElementById("Circle"+canvas.randomId)
			
			absolutePosition = getPosition(element);
			
			x = x - (absolutePosition.x + canvas.center[0]);
			y = y - (absolutePosition.y + canvas.center[1]);
			var radians = Math.atan2(x, y);
			
			radians = stepFunc(radians, canvas.calcAngleStep);
			updateCircle('CircleColored'+canvas.randomId, 'solid', canvas.color, radians);
			console.log("X is: "+Math.cos(radians) * (canvas.radius - 10) + canvas.center[0]+" Y is: "+Math.sin(radians) * (canvas.radius - 10) + canvas.center[1]+" Radians: "+radians);
			return {
				x: Math.sin(radians) * (canvas.radius - 10) + canvas.center[0],
				y: Math.cos(radians) * (canvas.radius - 10) + canvas.center[1]
			}
	}
	
	/**
	 * Method, that creates a value container for current
	 * value to be shown
	 * @method
	 */
	function createValue(){
		var divEl = document.createElement('li');
		divEl.id = "value"+canvas.randomId;
		divEl.innerHTML = "$0";
		document.getElementById(canvas.container+"Values").appendChild(divEl);
	}
	
	/**
	 * Function that retrieves an absolute position of given element.
	 * @function
	 */
	function getPosition(element) {
		var xPosition = 0;
		var yPosition = 0;
	  
		while(element) {
			xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
			yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
			element = element.offsetParent;
		}
		return { x: xPosition, y: yPosition };
	}
	
	return {
		create: createPrivate
	}
}